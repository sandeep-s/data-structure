# binary search alogrithm

"""
# the below function will take list and item and the functon will return the position if the item is found
# or it will return none if item is not found..
"""

def binary_search(list, item):
    low = 0
    high = len(list) - 1

    while low <= high:
        mid = int((low + high) / 2)
        guess = list[mid]

        if guess == item:
            return mid
        if guess < item:
            low = mid + 1
        else: 
            high = mid - 1

    return None

# test case of above binary search algorithm..
my_list = [2,3,5,6,8,9,10,20,32,33,45,100]

print(binary_search(my_list, 5))
print(binary_search(my_list, 45))


