# selection sort algorithm is used when you want to sort the given list in desired way and create the new list...
""" 
for example you have unsorted arr or list of integers and you wanted to sort that list form smallest integer to 
largest integer in this how you can sort that list by using selection sort is given below..
"""

def smallest_integer(arr):
    smallest = arr[0]
    smallest_index = 0
    
    for i in range(1, len(arr)):
        if arr[i] < smallest:
            smallest = arr[i]
            smallest_index = i
    return smallest_index


def selection_sort(arr):
    newarr = []
    
    for i in range(len(arr)):
        smallest_val_index = smallest_integer(arr)
        newarr.append(arr.pop(smallest_val_index))
    return newarr


# example test of above algorithm..
myarr = [2, 5, 4, 1, 3, 9, 7, 8]       # test array for our algorithm.
print(selection_sort(myarr)) # going to sort the array. and will return new sorted array [1, 2, 3, 4, 5, 7, 8, 9]



